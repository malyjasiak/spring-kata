package com.kata.father.query.repository;

import com.kata.father.model.entity.Father;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FatherQueryRepository extends JpaRepository<Father, Integer> {
}
