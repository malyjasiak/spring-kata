package com.kata.father.query.mapper;

import com.kata.father.query.dto.FatherDto;
import com.kata.father.model.entity.Father;
import org.mapstruct.Mapper;

@Mapper
public interface FatherQueryMapper {

    FatherDto toFatherDto(Father father);
}
