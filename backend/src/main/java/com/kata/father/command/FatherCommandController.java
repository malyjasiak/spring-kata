package com.kata.father.command;

import com.kata.father.command.dto.CreateFatherDto;
import com.kata.father.command.dto.FatherIdDto;
import com.kata.father.command.service.FatherCommandService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/fathers")
@RequiredArgsConstructor
public class FatherCommandController {

    private final FatherCommandService fatherCommandService;

    @PostMapping
    public FatherIdDto addFatherToFamily(@Validated @RequestBody CreateFatherDto createFatherDto) {
        return fatherCommandService.addFatherToFamily(createFatherDto);
    }
}
