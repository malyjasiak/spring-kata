package com.kata.father.command.service;

import com.kata.father.command.dto.CreateFatherDto;
import com.kata.father.command.dto.FatherIdDto;

public interface FatherCommandService {

    FatherIdDto addFatherToFamily(CreateFatherDto createFatherDto);
}
