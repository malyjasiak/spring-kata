package com.kata.father.command.dto;

import com.kata.family.command.dto.FamilyIdDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateFatherDto {

    @NotNull
    private FamilyIdDto family;
    @NotBlank
    private String firstName;
    private String secondName;
    @NotNull
    private ZonedDateTime birthDate;
    @Size(min = 11, max = 11)
    @Pattern(regexp = "[0-9]+")
    private String pesel;
}
