package com.kata.child.command.mapper;

import com.kata.child.command.dto.ChildIdDto;
import com.kata.child.command.dto.CreateChildDto;
import com.kata.child.model.entity.Child;
import com.kata.family.command.mapper.FamilyCommandMapper;
import org.mapstruct.Mapper;

@Mapper(uses = {FamilyCommandMapper.class})
public interface ChildCommandMapper {

    Child toChild(CreateChildDto child);
    ChildIdDto toChildIdDto(Child child);
}
