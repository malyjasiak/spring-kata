package com.kata.child.command.repository;

import com.kata.child.model.entity.Child;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChildCommandRepository extends JpaRepository<Child, Integer> {
}
