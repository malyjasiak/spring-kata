package com.kata.child.command.service.impl;

import com.kata.child.command.dto.ChildIdDto;
import com.kata.child.command.dto.CreateChildDto;
import com.kata.child.command.mapper.ChildCommandMapper;
import com.kata.child.command.service.ChildCommandService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ChildCommandServiceImpl implements ChildCommandService {

    private final ChildCommandMapper childCommandMapper;
    private final ChildCreator childCreator;

    @Override
    public ChildIdDto createChild(CreateChildDto createChildDto) {
        var child = childCommandMapper.toChild(createChildDto);
        var createdChild = childCreator.createChild(child);
        return childCommandMapper.toChildIdDto(createdChild);
    }
}
