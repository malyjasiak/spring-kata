package com.kata.child.command;

import com.kata.child.command.dto.ChildIdDto;
import com.kata.child.command.dto.CreateChildDto;
import com.kata.child.command.service.ChildCommandService;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/children")
@RequiredArgsConstructor
public class ChildCommandController {

    private final ChildCommandService childCommandService;

    @PostMapping
    public ChildIdDto createChild(@Validated @RequestBody CreateChildDto createChildDto) {
        return childCommandService.createChild(createChildDto);
    }
}
