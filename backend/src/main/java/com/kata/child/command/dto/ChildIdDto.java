package com.kata.child.command.dto;

import lombok.Data;

@Data
public class ChildIdDto {

    private int id;
}
