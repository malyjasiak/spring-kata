package com.kata.child.query.service;

import com.kata.child.query.dto.ChildDto;
import com.kata.child.query.dto.ChildSearchDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ChildQueryService {

    ChildDto findOneById(int id);
    Page<ChildDto> searchChild(ChildSearchDto childSearchDto, Pageable pageable);
}
