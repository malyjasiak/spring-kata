package com.kata.child.query.dto;

import com.kata.child.model.enumeration.Sex;
import lombok.Data;

@Data
public class ChildSearchDto {

    private String firstName;
    private String secondName;
    private String pesel;
    private Sex sex;
}
