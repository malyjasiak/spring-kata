package com.kata.child.query;

import com.kata.child.query.dto.ChildDto;
import com.kata.child.query.dto.ChildSearchDto;
import com.kata.child.query.service.ChildQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/children")
@RequiredArgsConstructor
public class ChildQueryController {

    private final ChildQueryService childQueryService;

    @GetMapping("/{id}")
    public ChildDto findOneById(@PathVariable Integer id) {
        return childQueryService.findOneById(id);
    }

    @GetMapping
    public Page<ChildDto> searchChild(ChildSearchDto s, Pageable p) {
        return childQueryService.searchChild(s, p);
    }
}
