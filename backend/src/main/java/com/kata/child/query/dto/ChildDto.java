package com.kata.child.query.dto;

import com.kata.child.model.enumeration.Sex;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ChildDto {

    private int id;
    private String firstName;
    private String secondName;
    private String pesel;
    private Sex sex;
}
