package com.kata.family.command.exception;

public class FamilyCreationException extends RuntimeException {

    public FamilyCreationException(Throwable throwable) {
        super(throwable);
    }
}
