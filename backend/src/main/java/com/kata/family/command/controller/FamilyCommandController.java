package com.kata.family.command.controller;

import com.kata.family.command.dto.FamilyIdDto;
import com.kata.family.command.service.FamilyCommandService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/families")
@RequiredArgsConstructor
public class FamilyCommandController {

    private final FamilyCommandService familyCommandService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public FamilyIdDto createFamily() {
        return familyCommandService.createFamily();
    }
}
