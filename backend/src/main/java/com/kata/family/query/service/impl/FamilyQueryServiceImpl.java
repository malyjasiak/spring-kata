package com.kata.family.query.service.impl;

import com.kata.family.query.exception.FamilyNotFoundException;
import com.kata.family.query.mapper.FamilyQueryMapper;
import com.kata.family.query.model.dto.FamilyDto;
import com.kata.family.query.model.view.FamilyViewDto;
import com.kata.family.query.repository.FamilyQueryRepository;
import com.kata.family.query.repository.FamilyViewRepository;
import com.kata.family.query.service.FamilyQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class FamilyQueryServiceImpl implements FamilyQueryService {

    private final FamilyQueryRepository familyQueryRepository;
    private final FamilyViewRepository familyViewRepository;
    private final FamilyQueryMapper familyQueryMapper;

    @Override
    public Page<FamilyViewDto> findAllFamilyView(Pageable pageable) {
        return familyViewRepository.findAll(pageable);
    }

    @Override
    public FamilyDto findOneById(int id) {
        return familyQueryRepository.findById(id)
                .map(familyQueryMapper::toFamilyDto)
                .orElseThrow(FamilyNotFoundException::new);
    }
}
