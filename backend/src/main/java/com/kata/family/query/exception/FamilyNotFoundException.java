package com.kata.family.query.exception;

public class FamilyNotFoundException extends RuntimeException {
    private final static String DEFAULT_MESSAGE = "Could not find family.";

    public FamilyNotFoundException() {
        super(DEFAULT_MESSAGE);
    }

    public FamilyNotFoundException(Throwable t) {
        super(DEFAULT_MESSAGE, t);
    }
 }
