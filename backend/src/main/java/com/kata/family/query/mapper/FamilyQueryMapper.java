package com.kata.family.query.mapper;

import com.kata.child.query.mapper.ChildQueryMapper;
import com.kata.family.model.entity.Family;
import com.kata.family.query.model.dto.FamilyDto;
import com.kata.father.query.mapper.FatherQueryMapper;
import org.mapstruct.Mapper;

@Mapper(uses = {FatherQueryMapper.class, ChildQueryMapper.class})
public interface FamilyQueryMapper {

    FamilyDto toFamilyDto(Family family);
    Family toFamily(FamilyDto family);
}
