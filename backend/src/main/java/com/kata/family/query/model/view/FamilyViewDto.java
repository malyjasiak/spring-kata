package com.kata.family.query.model.view;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "family_father_children")
public class FamilyViewDto {

    @Id
    private int id;
    private Integer fatherId;
    private String fatherFirstName;
    private String fatherSecondName;
    private String fatherPesel;
    private ZonedDateTime fatherBirthDate;
    private long childrenCount;
}
